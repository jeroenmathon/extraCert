# ANNOUNCHEMENT
## This project is to be migrated to C++ and will host a lot of new features and enhancements, such like:
- Certificate analysis
- Certificate/Key matching
- Root certificate extraction(Excluding root certificates)
- Certificate chain validation and reordering(Order by signed)
- PEM Generation
- PFX Generation(`openssl pkcs12 -export -out cert -inkey key -in crt` But perhaps a little bit more streamlined into the software)
- PFX Extraction

Needed packages: 
	- pyOpenSSL
