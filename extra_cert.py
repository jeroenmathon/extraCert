#! /usr/bin/python
# -*- coding: utf-8 -*i-
"""
Small programm to extract and show information from a certificate bundle.

Loads a certificate bundle, extracts all certificates and shows information
like subject (or CN), start and end dates.
"""

from argparse import ArgumentParser
from collections import namedtuple
from OpenSSL import crypto

parser = ArgumentParser('Sort and output non-root SSL certificates')
parser.add_argument('filepath', help='The file to extract certificates from')

Certificate = namedtuple('Certificate', ['subject', 'start_date', 'end_date'])

def extract_certs(filepath):
    # Read file line by line end extract certificate blocks into list.
    certificates = []
    current = []
    with open(filepath, 'r') as fp:
        for line in fp:
            # Reset `current` to empty list
            if '-----BEGIN CERTIFICATE-----' in line:
                current = []
            # Append current line to `current`
            current.append(line)
            # Join all lines of `current`, append to `certs`
            # and set `current` to none
            if '-----END CERTIFICATE-----' in line:
                certificates.append(''.join(current))
                current = []
    return certificates


def get_cert_info(data):
    # Return `Certificate` instance with CN, start and end date.
    cert = crypto.load_certificate(crypto.FILETYPE_PEM, data)
    subject = cert.get_subject()
    start = cert.get_notBefore().decode('utf-8')
    end = cert.get_notAfter().decode('utf-8')
    return Certificate(subject.CN or subject,
                       '%s/%s/%s' % (start[0:4], start[4:6], start[6:8]),
                       '%s/%s/%s' % (end[0:4], end[4:6], end[6:8]))


#TODO Filter out root certificates
# Main code execution
if __name__ == '__main__':
    args = parser.parse_args()
    for data in extract_certs(args.filepath):
        cert = get_cert_info(data)
        print('Subject: %s\nStart date: %s\nEnd date: %s\n' %
              (cert.subject, cert.start_date, cert.end_date))
